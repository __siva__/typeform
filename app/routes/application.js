import Route from '@ember/routing/route';

export default class ApplicationRoute extends Route {
  async model() {
    try {
      let response = await fetch('/data.json');
      let questions = await response.json() || {};
      return questions.questionnaire;
    } catch(err) {
      // Ideally, this should be logged using a logging framework(like Sentry)
      // to keep track of errors in production
      console.log(err);

      // TODO
      // send notificaiton
    }
  }
}
