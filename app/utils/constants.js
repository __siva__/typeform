const STATUS = {
  STARTED: 'started',
  FINISHED: 'finished'
};

const QUESTION_TYPES = {
  multipleChoice: 'multiple-choice',
  text: 'text'
};

export { STATUS, QUESTION_TYPES };
