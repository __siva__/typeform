import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { toUp, toDown } from 'ember-animated/transitions/move-over';
import { restartableTask, timeout } from 'ember-concurrency';

import { QUESTION_TYPES, STATUS } from '../utils/constants';
const DEBOUNCE_INTERVAL = 300;

export default class QuestionnaireComponent extends Component {
  status = STATUS;

  // This is used to keep track of questions that satisfied some condition.
  // For example, if question A jumped to C,
  // this object would be { C: A }
  // During inter-question transitions, this object is checked to see if a key
  // exists with the same value as the current question's ID.
  // If so, we redirect to the value of that key in this object.
  @tracked
  conditionalQuestions = {};

  @tracked
  currentQuestionIndex = null;

  @tracked
  questionsStatus = null;

  @tracked
  error = null;

  toUp = toUp;
  toDown = toDown;

  @tracked
  animation = this.toUp;

  get currentQuestion() {
    return this.args.questionnaire.questions[this.currentQuestionIndex];
  }

  get isFirstQuestion() {
    return this.currentQuestionIndex === 0;
  }

  get isLastQuestion() {
    return this.currentQuestionIndex === this.args.questionnaire.questions.length - 1;
  }

  @action
  goToQuestion(index) {
    this.animation = this.getAnimation(this.currentQuestionIndex, index);

    this.currentQuestionIndex = index;
  }

  getAnimation(current, next) {
    if (current < next) {
      return this.toUp;
    }

    return this.toDown;
  }

  @action
  goToPreviousQuestion() {
    if (!this.isFirstQuestion) {
      let destinationIndex = this.currentQuestionIndex - 1;
      // If this question has a match in the conditionalQuestions object,
      // redirect to the question from which this question jumped to ensure
      // we don't take the user to a question that was previously skipped.
      if (this.currentQuestionIndex in this.conditionalQuestions) {
        destinationIndex = this.conditionalQuestions[this.currentQuestionIndex];
      }

      this.goToQuestion(destinationIndex);
    }
  }

  // Unlike previous, next is treated like a submit button(to validate answers)
  @action
  goToNextQuestion() {
    this.validateAnswer() && this.handleSubmit();
  }

  // Returns the ID of a destination question if the jump conditions are met for the
  // current question.
  getQuestionToJumpTo() {
    let jumps = this.currentQuestion.jumps || [];
    for (let jump of jumps) {
      if (jump.conditions.length) {
        for (let condition of jump.conditions) {
          let field = condition.field;
          let question = this.args.questionnaire.questions.find(question => question.identifier === field);
          if (question.question_type === QUESTION_TYPES.multipleChoice) {
            let selectedOption = question.choices.find(option => option.selected === true);
            if (selectedOption) {
              if (selectedOption.value === condition.value) {
                let destinationQuestionIndex = this.args.questionnaire.questions.findIndex(question => question.identifier === jump.destination.id);
                if (destinationQuestionIndex !== -1) {
                  return destinationQuestionIndex
                }

                return;
              }
            }
          }
        }
      }
    }
  }

  isAnswerPresent() {
    if (!this.currentQuestion.required) {
      return true;
    }

    if (this.currentQuestion.question_type === QUESTION_TYPES.multipleChoice) {
      let selectedOption = this.currentQuestion.choices.find(option => option.selected === true);
      return selectedOption;
    }

    return this.currentQuestion.value;
  }

  @action
  setError(error) {
    this.error = error;
  }

  @action
  validateAnswer() {
    if (!this.isAnswerPresent()) {
      this.setError('Please answer the question before proceeding.');
      return;
    }

    this.setError(null);

    return true;
  }

  @restartableTask
  *_handleSubmit() {
    yield timeout(DEBOUNCE_INTERVAL);

    // currently in intro page
    if (this.questionsStatus !== this.status.STARTED) {
      this.questionsStatus = this.status.STARTED;
      this.currentQuestionIndex = 0;

      return;
    }

    // currently in last question, take to outro page
    if (this.isLastQuestion) {
      this.questionsStatus = this.status.FINISHED;
      return;
    }

    if (!this.validateAnswer()) {
      return;
    }

    // if a jump condition is satisfied, go to the destination given in the jump
    let destinationQuestionIndex = this.getQuestionToJumpTo();
    if (destinationQuestionIndex) {
      this.conditionalQuestions = {
        ...this.conditionalQuestions,
        [destinationQuestionIndex]: this.currentQuestionIndex
      }

      this.goToQuestion(destinationQuestionIndex);
      return;
    }

    this.goToQuestion(this.currentQuestionIndex + 1);
  }

  @action
  handleSubmit() {
    this._handleSubmit.perform();
  }
}
