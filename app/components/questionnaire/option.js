import Component from '@glimmer/component';
import { task, timeout } from 'ember-concurrency';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

const ANIMATION_DURATION = 500;

export default class QuestionnaireOptionComponent extends Component {
  @tracked
  shouldAnimate = false;

  @task({ drop: true })
  *_optionChanged(option) {
    this.shouldAnimate = true;
    yield timeout(ANIMATION_DURATION);
    this.shouldAnimate = false;

    this.args.optionChanged(option);
  }

  @action
  optionChanged(option) {
    this._optionChanged.perform(option);
  }
}
