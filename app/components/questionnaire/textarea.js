import Component from '@glimmer/component';
import { action } from '@ember/object';
import { set } from '@ember/object';

export default class QuestionnaireTextareaComponent extends Component {
  @action
  onInsert(element) {
    this.element = element;
    this.resize();
  }

  @action
  onChange(event) {
    this.resize();
    set(this.args.question, 'value', event.target.value);
  }

  @action
  resize() {
    if (this.element.value.replace(/\s/g, '').length > 0) {
      this.element.style.height = `${this.element.scrollHeight}px`;
    } else {
      this.element.style.height = 'auto';
    }
  }
}
