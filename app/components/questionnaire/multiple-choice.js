import Component from '@glimmer/component';
import { action } from '@ember/object';
import { set } from '@ember/object';

export default class QuestionnaireMultipleChoiceComponent extends Component {
  @action
  optionChanged(option) {
    let choice = this.args.question.choices.find(choice => choice.value === option);
    set(choice, 'selected', !choice.selected);
  }
}
