import Component from '@glimmer/component';

export default class QuestionnaireQuestionHeadingComponent extends Component {
  get formattedQuestionNumber() {
    return this.args.questionIndex + 1;
  }
}
