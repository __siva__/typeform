import Component from '@glimmer/component';
import { action } from '@ember/object';
import { restartableTask, timeout } from 'ember-concurrency';

import { QUESTION_TYPES } from '../../utils/constants';

const DEBOUNCE_INTERVAL = 500;

export default class QuestionnaireQuestionComponent extends Component {
  get isMultipleChoice() {
    return this.args.question.question_type === QUESTION_TYPES.multipleChoice &&
      this.args.question.multiple === 'true';
  }

  get isRadioButton() {
    return this.args.question.question_type === QUESTION_TYPES.multipleChoice &&
      this.args.question.multiple === 'false';
  }

  get isTextarea() {
    return this.args.question.question_type === QUESTION_TYPES.text &&
      this.args.question.multiline === 'true';
  }

  get isText() {
    return this.args.question.question_type === QUESTION_TYPES.text &&
      this.args.question.multiline === 'false';
  }

  get QuestionType() {
    if (this.isMultipleChoice) {
    return 'questionnaire/multiple-choice';
    }

    if (this.isRadioButton) {
      return 'questionnaire/radio-button';
    }

    if (this.isText) {
      return 'questionnaire/text';
    }

    if (this.isTextarea) {
      return 'questionnaire/textarea';
    }

    return 'questionnaire/text';
  }

  get submitLabel() {
    if (this.args.isLastQuestion) {
      return 'Finish';
    }

    return 'Submit';
  }

  get submitShortcut() {
    if (this.isTextarea) {
      return 'cmd+Enter';
    }

    return 'Enter';
  }

  @restartableTask
  *_handleSubmit() {
    yield timeout(DEBOUNCE_INTERVAL);
    
  }

  @action
  handleSubmit() {
    this.args.validateAnswer() && this.args.handleSubmit();
  }

  @action
  goToNextQuestion() {
    this.args.goToNextQuestion();
  }

  @action
  onError(error) {
    this.args.setError(error);
  }
}
