import Component from '@glimmer/component';
import { action } from '@ember/object';
import { set } from '@ember/object';

export default class QuestionnaireRadioButtonComponent extends Component {
  @action
  optionChanged(option) {
    let previousSelection = this.args.question.choices.find(choice => choice.selected === true && choice.value !== option);
    if (previousSelection) {
      set(previousSelection, 'selected', false);
    }

    let choice = this.args.question.choices.find(choice => choice.value === option);
    set(choice, 'selected', true);

    this.args.handleSubmit();
  }
}
