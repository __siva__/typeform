import Component from '@glimmer/component';
import { action } from '@ember/object';
import { set } from '@ember/object';

export default class QuestionnaireTextComponent extends Component {
  @action
  onChange(event) {
    set(this.args.question, 'value', event.target.value);
  }
}
