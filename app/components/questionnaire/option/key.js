import Component from '@glimmer/component';

export default class QuestionnaireOptionKeyComponent extends Component {
  get label() {
    let alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    return alphabet[this.args.index % 26];
  }

  get keyCode() {
    return this.label.toLowerCase();
  }
}
