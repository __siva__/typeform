import Component from '@glimmer/component';

export default class QuestionnaireSubmitComponent extends Component {
  label = this.args.label || 'Submit';
  keyboardShortcut = this.args.keyboardShortcut || 'Enter';

  iconsForKeys = {
    'Enter': '↵',
    'cmd': '⌘',
    'ctrl': '⌃',
    'alt': '⌥',
    'shift': '⇧'
  }

  get keyboardShortcutText() {
    let keys = this.keyboardShortcut.split('+');
    let iconKeys = keys.map(key => this.iconsForKeys[key] || key);
    return iconKeys.join(' + ');
  }
}
