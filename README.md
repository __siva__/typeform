# typeform

**Visit [http://typeform.siva.dev/](http://typeform.siva.dev/).**

## Prerequisites

You will need the following things properly installed on your computer.

* [Git](https://git-scm.com/)
* [Node.js](https://nodejs.org/) (with npm)
* [Ember CLI](https://ember-cli.com/)
* [Google Chrome](https://google.com/chrome/)

## Installation

* `git clone <repository-url>` this repository
* `cd typeform`
* `npm install`

## Running / Development

* `ember serve`
* Visit your app at [http://localhost:4200](http://localhost:4200).
* Visit your tests at [http://localhost:4200/tests](http://localhost:4200/tests).

### Code Generators

Make use of the many generators for code, try `ember help generate` for more details

### Running Tests

* `ember test`
* `ember test --server`

### Linting

* `npm run lint:hbs`
* `npm run lint:js`
* `npm run lint:js -- --fix`

### Building

* `ember build` (development)
* `ember build --environment production` (production)

### Usage

* Use the `Up` and `Down` arrow keys to navigate the questions.
* You can also use the navigation buttons provided in the bottom corner for this purpose.
* You can use the keyboard shortcuts displayed on the screen to submit and proceed to the next question.
* You can either click or press the corresponding keyboard characters to select an item in a multi-choice question.
