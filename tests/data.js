export default {
  "id": 40,
  "identifier": "ewBzTS",
  "name": "Privathaftpflichtversicherung",
  "questions": [
    {
      "question_type": "multiple-choice",
      "identifier": "list_12110962",
      "headline": "Wen möchtest Du versichern?",
      "description": null,
      "required": true,
      "multiple": "false",
      "choices": [
        {
          "label": "Meine Familie mit Kindern",
          "value": "Meine Familie mit Kindern",
          "selected": false
        },
        {
          "label": "Meine Familie ohne Kinder",
          "value": "Meine Familie ohne Kinder",
          "selected": false
        },
        {
          "label": "Mich ohne Kind",
          "value": "Mich ohne Kind",
          "selected": false
        },
        {
          "label": "Mich mit Kind",
          "value": "Mich mit Kind",
          "selected": false
        },
        {
          "label": "Mich und meinen Lebenspartner",
          "value": "Mich und meinen Lebenspartner",
          "selected": false
        }
      ],
      "jumps": []
    },
    {
      "question_type": "multiple-choice",
      "identifier": "list_12111610",
      "headline": "Bist Du Beamter oder im öffentlichen Dienst angestellt?",
      "description": null,
      "required": false,
      "multiple": "false",
      "choices": [
        {
          "label": "Ja",
          "value": "Ja",
          "selected": false
        },
        {
          "label": "Nein",
          "value": "Nein",
          "selected": false
        }
      ],
      "jumps": []
    },
    {
      "question_type": "multiple-choice",
      "identifier": "list_12111717",
      "headline": "Hast Du aktuell schon eine Privathaftpflichtversicherung?",
      "description": null,
      "required": false,
      "multiple": "false",
      "choices": [
        {
          "label": "Ja",
          "value": "Ja",
          "selected": false
        },
        {
          "label": "Nein",
          "value": "Nein",
          "selected": false
        }
      ],
      "jumps": [
        {
          "conditions": [
            {
              "field": "list_12111717",
              "value": "Ja"
            }
          ],
          "destination": {
            "id": "list_12111755"
          }
        },
        {
          "conditions": [
            {
              "field": "list_12111717",
              "value": "Nein"
            }
          ],
          "destination": {
            "id": "date_22039590"
          }
        }
      ]
    },
    {
      "question_type": "multiple-choice",
      "identifier": "list_12111755",
      "headline": "Wie viele Haftpflichtschäden hattest Du in den letzten 5 Jahren?",
      "description": null,
      "required": false,
      "multiple": "false",
      "choices": [
        {
          "label": "Keine",
          "value": "Keine",
          "selected": false
        },
        {
          "label": "1",
          "value": "1",
          "selected": false
        },
        {
          "label": "2",
          "value": "2",
          "selected": false
        },
        {
          "label": "3",
          "value": "3",
          "selected": false
        },
        {
          "label": "Mehr als 3",
          "value": "Mehr als 3",
          "selected": false
        }
      ],
      "jumps": []
    },
    {
      "question_type": "text",
      "identifier": "date_22039590",
      "headline": "Was wäre Dein Wunschtermin für den Beginn der Privathaftpflichtversicherung?",
      "description": null,
      "required": false,
      "multiline": "false",
      "jumps": []
    },
    {
      "question_type": "text",
      "identifier": "textarea_12110979",
      "headline": "Hast Du noch weitere Informationen oder Anmerkungen für uns?",
      "description": null,
      "required": false,
      "multiline": "true",
      "jumps": []
    }
  ],
  "description": "Um Dein persönliches Privathaftpflichtversicherungs-Angebot zu erstellen, benötigen wir noch ein paar Informationen von Dir.",
  "category_name_hyphenated": "Pri\u0026shy;vat\u0026shy;haft\u0026shy;pflicht"
}
