import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

module('Integration | Component | questionnaire/navigation', function(hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function(assert) {
    let isFirstQuestion = false;
    let isLastQuestion = false;
    let goToPreviousQuestion = function() {};
    let goToNextQuestion = function() {};

    this.setProperties({
      isFirstQuestion,
      isLastQuestion,
      goToPreviousQuestion,
      goToNextQuestion
    });

    await render(hbs`
      <Questionnaire::Navigation
        @isFirstQuestion={{this.isFirstQuestion}}
        @isLastQuestion={{this.isLastQuestion}}
        @goToPreviousQuestion={{this.goToPreviousQuestion}}
        @goToNextQuestion={{this.goToNextQuestion}}
      />
    `);

    assert.dom('[data-test-action="navigation__up"]').isNotDisabled();
    assert.dom('[data-test-action="navigation__down"]').isNotDisabled();
  });

  test('it disables previous button when in the first question', async function(assert) {
    let isFirstQuestion = true;
    let isLastQuestion = false;
    let goToPreviousQuestion = function() {};
    let goToNextQuestion = function() {};

    this.setProperties({
      isFirstQuestion,
      isLastQuestion,
      goToPreviousQuestion,
      goToNextQuestion
    });

    await render(hbs`
      <Questionnaire::Navigation
        @isFirstQuestion={{this.isFirstQuestion}}
        @isLastQuestion={{this.isLastQuestion}}
        @goToPreviousQuestion={{this.goToPreviousQuestion}}
        @goToNextQuestion={{this.goToNextQuestion}}
      />
    `);

    assert.dom('[data-test-action="navigation__up"]').isDisabled();
    assert.dom('[data-test-action="navigation__down"]').isNotDisabled();
  });

  test('it disables next button when in the last question', async function(assert) {
    let isFirstQuestion = false;
    let isLastQuestion = true;
    let goToPreviousQuestion = function() {};
    let goToNextQuestion = function() {};

    this.setProperties({
      isFirstQuestion,
      isLastQuestion,
      goToPreviousQuestion,
      goToNextQuestion
    });

    await render(hbs`
      <Questionnaire::Navigation
        @isFirstQuestion={{this.isFirstQuestion}}
        @isLastQuestion={{this.isLastQuestion}}
        @goToPreviousQuestion={{this.goToPreviousQuestion}}
        @goToNextQuestion={{this.goToNextQuestion}}
      />
    `);

    assert.dom('[data-test-action="navigation__up"]').isNotDisabled();
    assert.dom('[data-test-action="navigation__down"]').isDisabled();
  });

  test('it calls the respective actions when clicked', async function(assert) {
    let expectedAssertionsCount = 1;
    assert.expect(expectedAssertionsCount);

    let isFirstQuestion = false;
    let isLastQuestion = true;
    let goToPreviousQuestion = function() {
      assert.ok(true, 'goToPreviousQuestion should be called');
    };
    let goToNextQuestion = function() {
      assert.ok(false, 'goToNextQuestion should not be called');
    };

    this.setProperties({
      isFirstQuestion,
      isLastQuestion,
      goToPreviousQuestion,
      goToNextQuestion
    });

    await render(hbs`
      <Questionnaire::Navigation
        @isFirstQuestion={{this.isFirstQuestion}}
        @isLastQuestion={{this.isLastQuestion}}
        @goToPreviousQuestion={{this.goToPreviousQuestion}}
        @goToNextQuestion={{this.goToNextQuestion}}
      />
    `);

    await click('[data-test-action="navigation__up"]');
    await click('[data-test-action="navigation__down"]');
  });
});
