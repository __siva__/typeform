import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

module('Integration | Component | questionnaire/error', function(hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function(assert) {
    let error = 'This is an error';
    this.set('error', error);

    await render(hbs`<Questionnaire::Error @error={{this.error}} />`);
    assert.dom('[data-test-title="error"]').includesText(error);
  });
});
