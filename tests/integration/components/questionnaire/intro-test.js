import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

module('Integration | Component | questionnaire/intro', function(hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function(assert) {
    let name = 'name';
    let description = 'description';
    this.set('questionnaire', {
      name: name,
      description: description
    });

    this.set('handleSubmit', () => {});

    await render(hbs`
      <Questionnaire::Intro
        @questionnaire={{this.questionnaire}}
        @handleSubmit={{this.handleSubmit}}
      />
    `);

    assert.dom('[data-test-title="intro__name"]').includesText(name);
    assert.dom('[data-test-title="intro__description"]').includesText(description);
  });
});
