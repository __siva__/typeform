import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

module('Integration | Component | questionnaire/option/key', function(hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function(assert) {
    this.set('optionIndex', 2);
    this.set('onSelected', () => {});

    await render(hbs`
      <Questionnaire::Option::Key
        @index={{this.optionIndex}}
        @onSelected={{this.onSelected}}
      />
    `);

    assert.equal(this.element.textContent.trim(), 'C', 'option index should be converted to character');
  });
});
