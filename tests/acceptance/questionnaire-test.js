import { module, test } from 'qunit';
import { visit, currentURL, click, fillIn } from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';
import { keyDown } from 'ember-keyboard/test-support/test-helpers';

import {
  setupAnimationTest,
  animationsSettled,
  time,
  bounds,
} from 'ember-animated/test-support';

import data from '../data';

module('Acceptance | questionnaire', function(hooks) {
  setupApplicationTest(hooks);
  setupAnimationTest(hooks);

  async function submit() {
    await click('[data-test-action="submit"]');
    await animationsSettled();
  }

  test('required questions should not be skipped when submit is clicked', async function(assert) {
    await visit('/');

    assert.equal(currentURL(), '/');

    this.owner.lookup('controller:application').set('model', data);

    assert.dom('[data-test-title="intro"]').exists({ count: 1 });
    assert.dom('[data-test-title="question"]').doesNotExist();
    assert.dom('[data-test-title="outro"]').doesNotExist();

    await submit();

    assert.dom('[data-test-title="intro"]').doesNotExist();
    assert.dom('[data-test-title="question"]').exists({ count: 1 });
    assert.dom('[data-test-title="outro"]').doesNotExist();

    // Check for required state
    await click('[data-test-action="submit"]');
    assert.dom('[data-test-title="error"]').exists({ count: 1 });
  });

  test('required questions should not be skipped when arrow is clicked', async function(assert) {
    await visit('/');

    assert.equal(currentURL(), '/');

    this.owner.lookup('controller:application').set('model', data);

    await submit();

    await visit('/');
    await click('[data-test-action="navigation__down"]');
    assert.dom('[data-test-title="error"]').exists({ count: 1 });
  });

  test('required questions should not be skipped when keyboard shortcut is used', async function(assert) {
    await visit('/');

    assert.equal(currentURL(), '/');

    this.owner.lookup('controller:application').set('model', data);

    await submit();

    await visit('/');
    await keyDown('ArrowDown');
    assert.dom('[data-test-title="error"]').exists({ count: 1 });
  });

  test('goes to the next question when a radio button option is selected', async function(assert) {
    await visit('/');

    assert.equal(currentURL(), '/');

    this.owner.lookup('controller:application').set('model', data);

    await submit();

    await click('[data-test-option="option-0"]');
    await animationsSettled();

    assert.dom('[data-test-title="question"]').exists({ count: 1 });
    assert.dom('[data-test-title="question-2"]').exists({ count: 1 });
  });

  test('goes to the next question when a radio button option is selected through keyboard shortcut', async function(assert) {
    await visit('/');

    assert.equal(currentURL(), '/');

    this.owner.lookup('controller:application').set('model', data);

    await submit();

    await keyDown('a');
    await animationsSettled();

    assert.dom('[data-test-title="question"]').exists({ count: 1 });
    assert.dom('[data-test-title="question-2"]').exists({ count: 1 });
  });

  test('jump rules are not violated when there are no skips', async function(assert) {
    await visit('/');

    assert.equal(currentURL(), '/');

    let filteredData = {
      ...data,
      questions: [
        ...data.questions.slice(2)
      ]
    };

    this.owner.lookup('controller:application').set('model', filteredData);

    assert.dom('[data-test-title="intro"]').exists({ count: 1 });
    assert.dom('[data-test-title="question"]').doesNotExist();
    assert.dom('[data-test-title="outro"]').doesNotExist();

    await submit();

    await click('[data-test-option="option-0"]');
    await animationsSettled();
    assert.dom('[data-test-title="question-2"]').exists({ count: 1 });

    await keyDown('ArrowUp');
    await animationsSettled();
    assert.dom('[data-test-title="question-1"]').exists({ count: 1 });

    await keyDown('ArrowDown');
    await animationsSettled();
    assert.dom('[data-test-title="question-2"]').exists({ count: 1 });

    await keyDown('ArrowUp');
    await animationsSettled();
    assert.dom('[data-test-title="question-1"]').exists({ count: 1 });
  });

  test('jump rules are not violated when there are skips', async function(assert) {
    await visit('/');

    assert.equal(currentURL(), '/');

    let filteredData = {
      ...data,
      questions: [
        ...data.questions.slice(2)
      ]
    };

    this.owner.lookup('controller:application').set('model', filteredData);

    assert.dom('[data-test-title="intro"]').exists({ count: 1 });
    assert.dom('[data-test-title="question"]').doesNotExist();
    assert.dom('[data-test-title="outro"]').doesNotExist();

    await submit();

    await click('[data-test-option="option-1"]');
    await animationsSettled();
    assert.dom('[data-test-title="question-3"]').exists({ count: 1 });

    await click('[data-test-action="navigation__up"]');
    await animationsSettled();
    assert.dom('[data-test-title="question-1"]').exists({ count: 1 });

    await keyDown('ArrowDown');
    await animationsSettled();
    assert.dom('[data-test-title="question-3"]').exists({ count: 1 });
  });

  test('shows outro after the end of questions', async function(assert) {
    await visit('/');

    assert.equal(currentURL(), '/');

    let filteredData = {
      ...data,
      questions: [
        ...data.questions.slice(2)
      ]
    };

    this.owner.lookup('controller:application').set('model', filteredData);

    assert.dom('[data-test-title="intro"]').exists({ count: 1 });
    assert.dom('[data-test-title="question"]').doesNotExist();
    assert.dom('[data-test-title="outro"]').doesNotExist();

    await submit();

    for (let i = 0; i < filteredData.questions.length - 2; i++) {
      await click('[data-test-option="option-0"]');
      await animationsSettled();
    }

    for (let i = 0; i < 2; i++) {
      await fillIn('[data-test-title="input"]', 'text');
      await submit();
    }

    assert.dom('[data-test-title="outro"]').exists({ count: 1 });
  });

  test('animation direction is calculated correctly for down action', async function(assert) {
    await visit('/');

    assert.equal(currentURL(), '/');

    let filteredData = {
      ...data,
      questions: [
        ...data.questions.slice(2)
      ]
    };

    this.owner.lookup('controller:application').set('model', filteredData);

    await submit();

    time.pause();
    await click('[data-test-action="navigation__down"]');
    await time.advance(100);
    let positionOne = bounds(document.querySelector('[data-test-question-id="1"]')).top;
    await time.advance(100);
    let positionTwo = bounds(document.querySelector('[data-test-question-id="1"]')).top;
    assert.ok(positionOne > positionTwo, 'down action should move the question up');
  });

  test('animation direction is calculated correctly for up action', async function(assert) {
    await visit('/');

    assert.equal(currentURL(), '/');

    let filteredData = {
      ...data,
      questions: [
        ...data.questions.slice(2)
      ]
    };

    this.owner.lookup('controller:application').set('model', filteredData);

    await submit();
    await submit();

    time.pause();
    await click('[data-test-action="navigation__up"]');
    await time.advance(100);
    let positionOne = bounds(document.querySelector('[data-test-question-id="0"]')).top;
    await time.advance(100);
    let positionTwo = bounds(document.querySelector('[data-test-question-id="0"]')).top;
    assert.ok(positionOne < positionTwo, 'up action should move the question down');
  });
});
